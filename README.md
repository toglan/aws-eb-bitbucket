# CI/CD AWS Elastic Beanstalk - Bitbucket pipelines 

### Example 

This is a sample starter project for a bitbucket/git pipeline deployment of an Node.js AWS Elastic Beanstalk application.

### Run sample application locally
`npm start`

### Run sample application locally
`npm start`

### Test to deploy from local repo:
- Create a elastic beanstalk application 
- Rename .env.example to .env and make sure all values are valid for your application. Run the application with DOTENV=TRUE

$> `DOTENV=TRUE npm deploy`

 ### To use in your project
Modify and include the files
- bibucket-pipelines.yml
- deploy.js 

Then setup the bitbucket pipelines for your git branch. 