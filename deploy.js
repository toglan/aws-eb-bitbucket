if(process.env.DOTENV) {
	// start script with DOTENV=TRUE
	// to read from .env file
	require('dotenv').config();
}

const fs = require('fs');
const util = require('util');
const Promise = require('bluebird');
const AWS = require('aws-sdk');
const gitrev = require('git-rev');

const Bucket = process.env.AWS_DEPLOY_BUCKET;
const ApplicationName = process.env.EB_APPLICATION_NAME; 
const EnvironmentName = process.env.EB_APPLICATION_ENVIRONMENT;
const ApplicationArchive = `/tmp/Application.zip`;

const readFile = util.promisify(fs.readFile);

/* Async promisified ´version from gitrev */
const getVersionLabel = () => {
	return new Promise((resolve, reject) => {
		gitrev.short((githash) => {
			return resolve(githash);
		});
	});
};

/* Generate some acceptable S3 Bucket keyname from the git revision */
const getBucketKey = () => {
	return getVersionLabel()
		.then(versionLabel => `${ApplicationName}/${versionLabel}-bitbucket_build.zip`);
};

/* Upload a zipped application bundle to S3 */
const uploadToS3 = (bundle) => {
	const client = new AWS.S3();
	const putObject = util.promisify(client.putObject.bind(client));
	return Promise.all([
		getBucketKey(),
		readFile(bundle)
	])
	.then(([Key, Body]) => putObject({
		Bucket,
		Key,
		Body
	}));
};

/* Build a new EB application version */
const createNewEbVersion = () => {
	const client = new AWS.ElasticBeanstalk();
	const createApplicationVersion = util.promisify(client.createApplicationVersion.bind(client));
	return Promise.all([
			getVersionLabel(),
			getBucketKey()
		])
		.then(([
			VersionLabel, 
			S3Key
		]) => createApplicationVersion({
			ApplicationName,
			VersionLabel,
			Description: 'New bitbucket build',
			SourceBundle: {
				S3Bucket: Bucket,
				S3Key
			},
			Process: true
		}));
};

/* Deploy a built EB application */
const deployNewEbVersion = () => {
	const client = new AWS.ElasticBeanstalk();
	const updateEnvironment = util.promisify(client.updateEnvironment.bind(client));
	return getVersionLabel()
		.then(VersionLabel => updateEnvironment({
		ApplicationName,
		EnvironmentName,
		VersionLabel
	}));
};

/* Get the current EB application processing status */
const getProcessingStatus = () => {
	const client = new AWS.ElasticBeanstalk();
	const describeApplicationVersions = util.promisify(client.describeApplicationVersions.bind(client));
	return getVersionLabel()
		.then(VersionLabel => describeApplicationVersions({
			ApplicationName,
			MaxRecords: 1,
			VersionLabels: [VersionLabel]
		}))
		.then(({ ApplicationVersions }) => {
			const [ version ] = ApplicationVersions;
			const { Status } = version;
			return Status.toUpperCase();
		});
};

/* Whait for the EB build step to complete */
const awaitProcessingCompleted = () => {
	return new Promise((resolve, reject) => {
		return getProcessingStatus()
			.then(status => {
				switch (status) {
					case 'FAILED':
						return reject('Processing Application Version Failed');
					case 'PROCESSED':
						return resolve(true);
					default:
						setTimeout(() => {
							resolve(awaitProcessingCompleted());
						}, 1000);
				}
			});
	});
};

/* Run the deployment steps */
uploadToS3(ApplicationArchive)
	.then(createNewEbVersion)
	.then(awaitProcessingCompleted)
	.then(deployNewEbVersion)
	.then(console.info)
	.catch(e => {
		console.error(e);
		process.exit(1);
	});