const express = require('express');
const PORT = process.env.PORT || 3000;
const index = require('./routes/index');
const app = express();


app.use((req, res, next) => {
    console.log(req.method, req.path);
    next();
});

app.use('/', index);

app.listen(PORT, (err) => {
    if(err) {
        throw err;
    }
    console.log(`App listening on ${PORT}`);
});